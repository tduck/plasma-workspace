# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2010, 2012, 2013, 2014, 2015, 2018, 2020, 2024 Eloy Cuadra <ecuadra@eloihr.net>
# Javier Vinal <fjvinal@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: plasma_engine_soliddevice\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-24 00:38+0000\n"
"PO-Revision-Date: 2024-02-24 20:15+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Spanish\n"
"X-Poedit-Country: SPAIN\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: soliddeviceengine.cpp:112
msgid "Parent UDI"
msgstr "UDI padre"

#: soliddeviceengine.cpp:113
msgid "Vendor"
msgstr "Fabricante"

#: soliddeviceengine.cpp:114
msgid "Product"
msgstr "Producto"

#: soliddeviceengine.cpp:115
msgid "Description"
msgstr "Descripción"

#: soliddeviceengine.cpp:116
msgid "Icon"
msgstr "Icono"

#: soliddeviceengine.cpp:117 soliddeviceengine.cpp:594
msgid "Emblems"
msgstr "Emblemas"

#: soliddeviceengine.cpp:118 soliddeviceengine.cpp:476
#: soliddeviceengine.cpp:482 soliddeviceengine.cpp:495
msgid "State"
msgstr "Estado"

#: soliddeviceengine.cpp:119 soliddeviceengine.cpp:477
#: soliddeviceengine.cpp:483 soliddeviceengine.cpp:491
#: soliddeviceengine.cpp:493
msgid "Operation result"
msgstr "Resultado de la operación"

#: soliddeviceengine.cpp:120
msgid "Timestamp"
msgstr "Marca de tiempo"

#: soliddeviceengine.cpp:128
msgid "Processor"
msgstr "Procesador"

#: soliddeviceengine.cpp:129
msgid "Number"
msgstr "Número"

#: soliddeviceengine.cpp:130
msgid "Max Speed"
msgstr "Velocidad máx."

#: soliddeviceengine.cpp:131
msgid "Can Change Frequency"
msgstr "Puede cambiar la frecuencia"

#: soliddeviceengine.cpp:139
msgid "Block"
msgstr "Bloque"

#: soliddeviceengine.cpp:140
msgid "Major"
msgstr "Mayor"

#: soliddeviceengine.cpp:141
msgid "Minor"
msgstr "Menor"

#: soliddeviceengine.cpp:142
msgid "Device"
msgstr "Dispositivo"

#: soliddeviceengine.cpp:150
msgid "Storage Access"
msgstr "Acceso de almacenamiento"

#: soliddeviceengine.cpp:151 soliddeviceengine.cpp:507
#: soliddeviceengine.cpp:608
msgid "Accessible"
msgstr "Accesible"

#: soliddeviceengine.cpp:152 soliddeviceengine.cpp:508
msgid "File Path"
msgstr "Ruta del archivo"

#: soliddeviceengine.cpp:167
msgid "Storage Drive"
msgstr "Dispositivo de almacenamiento"

#: soliddeviceengine.cpp:170
msgid "Ide"
msgstr "IDE"

#: soliddeviceengine.cpp:170
msgid "Usb"
msgstr "USB"

#: soliddeviceengine.cpp:170
msgid "Ieee1394"
msgstr "IEEE1394"

#: soliddeviceengine.cpp:171
msgid "Scsi"
msgstr "SCSI"

#: soliddeviceengine.cpp:171
msgid "Sata"
msgstr "SATA"

#: soliddeviceengine.cpp:171
msgid "Platform"
msgstr "Plataforma"

#: soliddeviceengine.cpp:173
msgid "Hard Disk"
msgstr "Disco duro"

#: soliddeviceengine.cpp:173
msgid "Cdrom Drive"
msgstr "Unidad de CDROM"

#: soliddeviceengine.cpp:173
msgid "Floppy"
msgstr "Disquetera"

#: soliddeviceengine.cpp:174
msgid "Tape"
msgstr "Cinta"

#: soliddeviceengine.cpp:174
msgid "Compact Flash"
msgstr "Compact Flash"

#: soliddeviceengine.cpp:174
msgid "Memory Stick"
msgstr "Memory Stick"

#: soliddeviceengine.cpp:175
msgid "Smart Media"
msgstr "Smart Media"

#: soliddeviceengine.cpp:175
msgid "SdMmc"
msgstr "SdMmc"

#: soliddeviceengine.cpp:175
msgid "Xd"
msgstr "XD"

#: soliddeviceengine.cpp:177
msgid "Bus"
msgstr "Bus"

#: soliddeviceengine.cpp:178
msgid "Drive Type"
msgstr "Tipo de unidad"

#: soliddeviceengine.cpp:179 soliddeviceengine.cpp:192
#: soliddeviceengine.cpp:360 soliddeviceengine.cpp:374
msgid "Removable"
msgstr "Extraíble"

#: soliddeviceengine.cpp:180 soliddeviceengine.cpp:193
#: soliddeviceengine.cpp:361 soliddeviceengine.cpp:375
msgid "Hotpluggable"
msgstr "Se puede conectar en caliente"

#: soliddeviceengine.cpp:202
msgid "Optical Drive"
msgstr "Unidad óptica"

#: soliddeviceengine.cpp:207
msgid "CD-R"
msgstr "CD-R"

#: soliddeviceengine.cpp:210
msgid "CD-RW"
msgstr "CD-RW"

#: soliddeviceengine.cpp:213
msgid "DVD"
msgstr "DVD"

#: soliddeviceengine.cpp:216
msgid "DVD-R"
msgstr "DVD-R"

#: soliddeviceengine.cpp:219
msgid "DVD-RW"
msgstr "DVD-RW"

#: soliddeviceengine.cpp:222
msgid "DVD-RAM"
msgstr "DVD-RAM"

#: soliddeviceengine.cpp:225
msgid "DVD+R"
msgstr "DVD+R"

#: soliddeviceengine.cpp:228
msgid "DVD+RW"
msgstr "DVD+RW"

#: soliddeviceengine.cpp:231
msgid "DVD+DL"
msgstr "DVD+DL"

#: soliddeviceengine.cpp:234
msgid "DVD+DLRW"
msgstr "DVD+DLRW"

#: soliddeviceengine.cpp:237
msgid "BD"
msgstr "BD"

#: soliddeviceengine.cpp:240
msgid "BD-R"
msgstr "BD-R"

#: soliddeviceengine.cpp:243
msgid "BD-RE"
msgstr "BD-RE"

#: soliddeviceengine.cpp:246
msgid "HDDVD"
msgstr "HDDVD"

#: soliddeviceengine.cpp:249
msgid "HDDVD-R"
msgstr "HDDVD-R"

#: soliddeviceengine.cpp:252
msgid "HDDVD-RW"
msgstr "HDDVD-RW"

#: soliddeviceengine.cpp:254
msgid "Supported Media"
msgstr "Medios soportados"

#: soliddeviceengine.cpp:256
msgid "Read Speed"
msgstr "Velocidad de lectura"

#: soliddeviceengine.cpp:257
msgid "Write Speed"
msgstr "Velocidad de escritura"

#: soliddeviceengine.cpp:265
msgid "Write Speeds"
msgstr "Velocidades de escritura"

#: soliddeviceengine.cpp:273
msgid "Storage Volume"
msgstr "Volumen de almacenamiento"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Other"
msgstr "Otros"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Unused"
msgstr "No usado"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "File System"
msgstr "Sistema de archivos"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Partition Table"
msgstr "Tabla de particiones"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soliddeviceengine.cpp:276
#, kde-format
msgid "Encrypted"
msgstr "Cifrado"

#: soliddeviceengine.cpp:279 soliddeviceengine.cpp:281
msgid "Usage"
msgstr "Uso"

#: soliddeviceengine.cpp:281
#, kde-format
msgid "Unknown"
msgstr "Desconocido"

#: soliddeviceengine.cpp:284
msgid "Ignored"
msgstr "Ignorado"

#: soliddeviceengine.cpp:285
msgid "File System Type"
msgstr "Tipo de sistema de archivos"

#: soliddeviceengine.cpp:286
msgid "Label"
msgstr "Etiqueta"

#: soliddeviceengine.cpp:287
msgid "UUID"
msgstr "UUID"

#: soliddeviceengine.cpp:296
msgid "Encrypted Container"
msgstr "Contenedor cifrado"

#: soliddeviceengine.cpp:308
msgid "OpticalDisc"
msgstr "Disco óptico"

#: soliddeviceengine.cpp:314
msgid "Audio"
msgstr "Audio"

#: soliddeviceengine.cpp:317
msgid "Data"
msgstr "Datos"

#: soliddeviceengine.cpp:320
msgid "Video CD"
msgstr "Vídeo CD"

#: soliddeviceengine.cpp:323
msgid "Super Video CD"
msgstr "Super Vídeo CD"

#: soliddeviceengine.cpp:326
msgid "Video DVD"
msgstr "Vídeo DVD"

#: soliddeviceengine.cpp:329
msgid "Video Blu Ray"
msgstr "Vídeo Blu Ray"

#: soliddeviceengine.cpp:331
msgid "Available Content"
msgstr "Contenido disponible"

#: soliddeviceengine.cpp:334
msgid "Unknown Disc Type"
msgstr "Tipo de disco desconocido"

#: soliddeviceengine.cpp:334
msgid "CD Rom"
msgstr "CD ROM"

#: soliddeviceengine.cpp:334
msgid "CD Recordable"
msgstr "CD Grabable"

#: soliddeviceengine.cpp:335
msgid "CD Rewritable"
msgstr "CD Regrabable"

#: soliddeviceengine.cpp:335
msgid "DVD Rom"
msgstr "DVD ROM"

#: soliddeviceengine.cpp:335
msgid "DVD Ram"
msgstr "DVD RAM"

#: soliddeviceengine.cpp:336
msgid "DVD Recordable"
msgstr "DVD Grabable"

#: soliddeviceengine.cpp:336
msgid "DVD Rewritable"
msgstr "DVD Regrabable"

#: soliddeviceengine.cpp:337
msgid "DVD Plus Recordable"
msgstr "DVD Plus grabable"

#: soliddeviceengine.cpp:337
msgid "DVD Plus Rewritable"
msgstr "DVD Plus regrabable"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Recordable Duallayer"
msgstr "DVD Plus grabable de doble capa"

#: soliddeviceengine.cpp:338
msgid "DVD Plus Rewritable Duallayer"
msgstr "DVD Plus regrabable de doble capa"

#: soliddeviceengine.cpp:339
msgid "Blu Ray Rom"
msgstr "Blu Ray ROM"

#: soliddeviceengine.cpp:339
msgid "Blu Ray Recordable"
msgstr "Blu Ray grabable"

#: soliddeviceengine.cpp:340
msgid "Blu Ray Rewritable"
msgstr "Blu Ray regrabable"

#: soliddeviceengine.cpp:340
msgid "HD DVD Rom"
msgstr "HD DVD ROM"

#: soliddeviceengine.cpp:341
msgid "HD DVD Recordable"
msgstr "HD DVD grabable"

#: soliddeviceengine.cpp:341
msgid "HD DVD Rewritable"
msgstr "HD DVD regrabable"

#: soliddeviceengine.cpp:343
msgid "Disc Type"
msgstr "Tipo de disco"

#: soliddeviceengine.cpp:344
msgid "Appendable"
msgstr "Permite añadir"

#: soliddeviceengine.cpp:345
msgid "Blank"
msgstr "En blanco"

#: soliddeviceengine.cpp:346
msgid "Rewritable"
msgstr "Regrabable"

#: soliddeviceengine.cpp:347
msgid "Capacity"
msgstr "Capacidad"

#: soliddeviceengine.cpp:355
msgid "Camera"
msgstr "Cámara"

#: soliddeviceengine.cpp:357 soliddeviceengine.cpp:371
msgid "Supported Protocols"
msgstr "Protocolos permitidos"

#: soliddeviceengine.cpp:358 soliddeviceengine.cpp:372
msgid "Supported Drivers"
msgstr "Controladores permitidos"

#: soliddeviceengine.cpp:369
msgid "Portable Media Player"
msgstr "Reproductor de medios portátil"

#: soliddeviceengine.cpp:383
msgid "Battery"
msgstr "Batería"

#: soliddeviceengine.cpp:386
msgid "Unknown Battery"
msgstr "Batería desconocida"

#: soliddeviceengine.cpp:386
msgid "PDA Battery"
msgstr "Batería de la PDA"

#: soliddeviceengine.cpp:386
msgid "UPS Battery"
msgstr "Batería del SAI"

#: soliddeviceengine.cpp:387
msgid "Primary Battery"
msgstr "Batería primaria"

#: soliddeviceengine.cpp:387
msgid "Mouse Battery"
msgstr "Batería del ratón"

#: soliddeviceengine.cpp:388
msgid "Keyboard Battery"
msgstr "Batería del teclado"

#: soliddeviceengine.cpp:388
msgid "Keyboard Mouse Battery"
msgstr "Batería del ratón y del teclado"

#: soliddeviceengine.cpp:389
msgid "Camera Battery"
msgstr "Batería de la cámara"

#: soliddeviceengine.cpp:389
msgid "Phone Battery"
msgstr "Batería del teléfono"

#: soliddeviceengine.cpp:389
msgid "Monitor Battery"
msgstr "Batería del monitor"

#: soliddeviceengine.cpp:390
msgid "Gaming Input Battery"
msgstr "Batería del dispositivo de entrada para juegos"

#: soliddeviceengine.cpp:390
msgid "Bluetooth Battery"
msgstr "Batería Bluetooth"

#: soliddeviceengine.cpp:391
msgid "Tablet Battery"
msgstr "Batería de la tableta"

#: soliddeviceengine.cpp:391
msgid "Headphone Battery"
msgstr "Batería de los cascos"

#: soliddeviceengine.cpp:392
msgid "Headset Battery"
msgstr "Batería de los auriculares"

#: soliddeviceengine.cpp:392
msgid "Touchpad Battery"
msgstr "Batería del panel táctil"

#: soliddeviceengine.cpp:395
msgid "Not Charging"
msgstr "No está en carga"

#: soliddeviceengine.cpp:395
msgid "Charging"
msgstr "Cargando"

#: soliddeviceengine.cpp:395
msgid "Discharging"
msgstr "Descargando"

#: soliddeviceengine.cpp:396
msgid "Fully Charged"
msgstr "Totalmente cargada"

#: soliddeviceengine.cpp:398
msgid "Plugged In"
msgstr "Conectado"

#: soliddeviceengine.cpp:399
msgid "Type"
msgstr "Tipo"

#: soliddeviceengine.cpp:400
msgid "Charge Percent"
msgstr "Porcentaje de carga"

#: soliddeviceengine.cpp:401
msgid "Rechargeable"
msgstr "Recargable"

#: soliddeviceengine.cpp:402
msgid "Charge State"
msgstr "Estado de la carga"

#: soliddeviceengine.cpp:427
msgid "Type Description"
msgstr "Descripción de tipo"

#: soliddeviceengine.cpp:432
msgid "Device Types"
msgstr "Tipos de dispositivos"

#: soliddeviceengine.cpp:456 soliddeviceengine.cpp:555
msgid "Size"
msgstr "Tamaño"

#: soliddeviceengine.cpp:535
#, kde-format
msgid "Filesystem is not responding"
msgstr "El sistema de archivos no responde"

#: soliddeviceengine.cpp:535
#, kde-format
msgid "Filesystem mounted at '%1' is not responding"
msgstr "El sistema de archivos montado en «%1» no responde"

#: soliddeviceengine.cpp:553
msgid "Free Space"
msgstr "Espacio libre"

#: soliddeviceengine.cpp:554
msgid "Free Space Text"
msgstr "Texto del espacio libre"

#: soliddeviceengine.cpp:556
msgid "Size Text"
msgstr "Texto del tamaño"

#: soliddeviceengine.cpp:582
msgid "Temperature"
msgstr "Temperatura"

#: soliddeviceengine.cpp:583
msgid "Temperature Unit"
msgstr "Unidad de temperatura"

#: soliddeviceengine.cpp:627 soliddeviceengine.cpp:631
msgid "In Use"
msgstr "En uso"

#~ msgid "Network Interface"
#~ msgstr "Interfaz de red"

#~ msgid "Interface Name"
#~ msgstr "Nombre de la interfaz"

#~ msgid "Wireless"
#~ msgstr "Inalámbrica"

#~ msgid "Hardware Address"
#~ msgstr "Dirección hardware"

#~ msgid "MAC Address"
#~ msgstr "Dirección MAC"

#~ msgid "AC Adapter"
#~ msgstr "Adaptador AC"

#~ msgid "Button"
#~ msgstr "Botón"

#~ msgid "Lid Button"
#~ msgstr "Botón de la tapa"

#~ msgid "Power Button"
#~ msgstr "Botón de encendido"

#~ msgid "Sleep Button"
#~ msgstr "Botón «Dormir»"

#~ msgid "Unknown Button Type"
#~ msgstr "Tipo de botón desconocido"

#~ msgid "Has State"
#~ msgstr "Tiene el estado"

#~ msgid "State Value"
#~ msgstr "Valor del estado"

#~ msgid "Pressed"
#~ msgstr "Pulsado"

#~ msgid "Audio Interface"
#~ msgstr "Interfaz de audio"

#~ msgid "ALSA"
#~ msgstr "ALSA"

#~ msgid "Open Sound System"
#~ msgstr "Open Sound System"

#~ msgid "Unknown Audio Driver"
#~ msgstr "Controlador de audio desconocido"

#~ msgid "Driver"
#~ msgstr "Controlador"

#~ msgid "Driver Handle"
#~ msgstr "Manejador del controlador"

#~ msgid "Name"
#~ msgstr "Nombre"

#~ msgid "Unknown Audio Interface Type"
#~ msgstr "Tipo de interfaz de audio desconocido"

#~ msgid "Audio Control"
#~ msgstr "Control de audio"

#~ msgid "Audio Input"
#~ msgstr "Entrada de audio"

#~ msgid "Audio Output"
#~ msgstr "Salida de audio"

#~ msgid "Audio Device Type"
#~ msgstr "Tipo de dispositivo de audio"

#~ msgid "Internal Soundcard"
#~ msgstr "Tarjeta de sonido interna"

#~ msgid "USB Soundcard"
#~ msgstr "Tarjeta de sonido USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "Tarjeta de sonido Firewire"

#~ msgid "Headset"
#~ msgstr "Auriculares"

#~ msgid "Modem"
#~ msgstr "Módem"

#~ msgid "Soundcard Type"
#~ msgstr "Tipo de tarjeta de sonido"

#~ msgid "DVB Interface"
#~ msgstr "Interfaz DVB"

#~ msgid "Device Adapter"
#~ msgstr "Adaptador de dispositivo"

#~ msgid "DVB Unknown"
#~ msgstr "DVB desconocido"

#~ msgid "DVB Audio"
#~ msgstr "DVB Audio"

#~ msgid "DVB Ca"
#~ msgstr "DVB Ca"

#~ msgid "DVB Demux"
#~ msgstr "DVB Demux"

#~ msgid "DVB DVR"
#~ msgstr "DVB DVR"

#~ msgid "DVB Frontend"
#~ msgstr "Interfaz DVB"

#~ msgid "DVB Net"
#~ msgstr "Red DVB"

#~ msgid "DVB OSD"
#~ msgstr "DVB OSD"

#~ msgid "DVB Sec"
#~ msgstr "DVB Sec"

#~ msgid "DVB Video"
#~ msgstr "DVB Vídeo"

#~ msgid "DVB Device Type"
#~ msgstr "Tipo de dispositivo DVB"

#~ msgid "Device Index"
#~ msgstr "Índice del dispositivo"

#~ msgid "Video"
#~ msgstr "Vídeo"

#~ msgid "Driver Handles"
#~ msgstr "Manejadores de dispositivos"
